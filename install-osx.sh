#!/bin/bash

cp -rp Release-OSX/bigsh0t-2.1-osx/lib/frei0r-1/* /Applications/Shotcut.app/Contents/PlugIns/frei0r-1/
cp -rp Release-OSX/bigsh0t-2.1-osx/shotcut/share/shotcut/qml/filters/* /Applications/Shotcut.app/Contents/Resources/shotcut/qml/filters/
